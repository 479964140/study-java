package com.chensi.core;

/**
 * @author si.chen
 * @date 2020/4/9 12:09
 */
public class TestTransferValue {

    public void change1(int age) {
        age = 30;
    }

    public void change2(String str) {
        str = "x";
    }

    public void change3(Person p) {
        p.setName("test");
    }

    public void change4(Integer i) {
        i = 18;
    }

    public static void main(String[] args) {
        TestTransferValue obj = new TestTransferValue();
        int age = 20;
        obj.change1(age);
        System.out.println("age---" + age);
        //20

        String str = "abc";
        obj.change2(str);
        System.out.println("str---" + str);
        //abc

        Integer a = 12;
        obj.change4(a);
        System.out.println("integer---" + a);
        //12

        //----------------
        Person p = new Person();
        obj.change3(p);
        System.out.println("person---" + p.getName());
        //test
    }
}

class Person {
    int age;
    String name;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
