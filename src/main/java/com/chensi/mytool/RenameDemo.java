package com.chensi.mytool;


import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Arrays;

public class RenameDemo {

    public static void main(String[] args) {
        File file = new File("D:\\学习\\Dubbo---周瑜");
        File[] f = file.listFiles();
        assert f != null;
        Arrays.asList(f).forEach(item -> {
            if (StringUtils.startsWith(item.getName(), "[")) {
                //截取后面的部分
                String fileName = StringUtils.substringAfter(item.getName(), "_");
                //新文件路径+文件名
                String newFileName = item.getParent() + File.separator + fileName;
                //新文件
                File newFile = new File(newFileName);
                //指向新文件
                item.renameTo(newFile);
            }
        });
    }

}
