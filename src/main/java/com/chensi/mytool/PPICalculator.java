package com.chensi.mytool;

/**
 * ppi计算器
 * PPI=√（X^2+Y^2）/ Z （X：长度像素数；Y：宽度像素数；Z：屏幕寸数大小）。
 * 5120*2880,3840*2160,2560*1600(13.3)
 *
 * @author si.chen
 * @date 2020/6/2 13:32
 */
public class PPICalculator {

    public static void main(String[] args) {
        double x = 2560;
        double y = 1600;
        double z = 13.3;
        double ppi = Math.sqrt(x * x + y * y) / z;
        System.out.println("PPI=" + ppi);
    }
}
