package com.chensi.gc;

/**
 * 递归死循环
 */
public class StackOverFlowErrorDemo {

    public static void main(String[] args) {
        stackOver();
    }

    private static void stackOver() {
        stackOver();
    }
}
