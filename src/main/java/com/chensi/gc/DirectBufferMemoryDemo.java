package com.chensi.gc;

import sun.misc.VM;

import java.nio.ByteBuffer;

/**
 * 堆外内存溢出
 * -XX:MaxDirectMemorySize=10m
 */
public class DirectBufferMemoryDemo {

    public static void main(String[] args) {
        System.out.println(VM.maxDirectMemory());//默认物理内存的1/4
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(11 * 1024 * 1024);
    }


}
