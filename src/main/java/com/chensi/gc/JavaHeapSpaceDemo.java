package com.chensi.gc;

import java.util.ArrayList;
import java.util.List;

/**
 * 无限创建大对象
 * -Xms10m -Xmx10m
 */
public class JavaHeapSpaceDemo {

    byte[] bytes = new byte[1 * 1024 * 1024];

    public static void main(String[] args) {
        List<JavaHeapSpaceDemo> list = new ArrayList<>();
        while (true) {
            JavaHeapSpaceDemo demo = new JavaHeapSpaceDemo();
            list.add(demo);
            System.out.println("111");
        }
    }
}
