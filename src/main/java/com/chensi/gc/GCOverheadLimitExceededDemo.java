package com.chensi.gc;

import java.util.ArrayList;
import java.util.List;

/**
 * GC来不及回收又打满
 * -Xms10m -Xmx10m
 */
public class GCOverheadLimitExceededDemo {

    public static void main(String[] args) {
        int i = 0;
        List<String> list = new ArrayList<>();

        try {
            while (true) {
                //new String都是在堆上创建字符串对象。当调用 intern() 方法时，
                // 编译器会将字符串添加到常量池中（stringTable维护），并返回指向该常量的引用
                list.add(String.valueOf(++i).intern());//先塞到常量池，再从常量池中取值
            }
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println("=================" + i);
            throw e;
        }

    }
}
