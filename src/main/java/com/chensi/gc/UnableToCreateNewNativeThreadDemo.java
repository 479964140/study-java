package com.chensi.gc;

import java.util.concurrent.TimeUnit;

/**
 * 无限创建线程会溢出
 */
public class UnableToCreateNewNativeThreadDemo {

    public static void main(String[] args) {

        for (int i = 0; ; i++) {
            System.out.println("i=" + i);
            new Thread(() -> {
                try {
                    TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, "" + i).start();
        }
    }
}
