package com.chensi.concurrency.zhouyang;

import java.util.concurrent.TimeUnit;

/**
 * a线程修改了MyData的number，其他线程会立马更新
 */
public class VolatileDemo {

    public static void main(String[] args) {
        MyData data = new MyData();

        //aaa线程
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "\t come in");
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            data.addTo60();
            System.out.println(Thread.currentThread().getName() + "\t update to " + data.number);
        }, "AAA").start();

        //主线程
        while (data.number == 0) {

        }
        System.out.println("main线程完毕");
    }
}

class MyData {

    //int number = 0;
    volatile int number = 0;

    public void addTo60() {
        this.number = 60;
    }
}
