package com.chensi.concurrency.zhouyang;

import lombok.*;

import java.util.concurrent.atomic.AtomicReference;

public class AtomicReferenceDemo {

    public static void main(String[] args) {

        AtomicReference<User> atomicReference = new AtomicReference<>();

        User user1 = new User("aaa", 1);
        User user2 = new User("bbb", 1);

        atomicReference.set(user1);

        System.out.println(atomicReference.compareAndSet(user1, user2) + "\t" + atomicReference.get());
        System.out.println(atomicReference.compareAndSet(user1, user2) + "\t" + atomicReference.get());

    }
}

@AllArgsConstructor
@Data
class User {
    String username;
    int age;
}