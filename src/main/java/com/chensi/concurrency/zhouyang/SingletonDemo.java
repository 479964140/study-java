package com.chensi.concurrency.zhouyang;

/**
 * 并发下的单例模式
 */
public class SingletonDemo {

    private SingletonDemo() {
        System.out.println(Thread.currentThread().getName() + "\t 执行构造方法");
    }

    private static volatile SingletonDemo instance = null;

    public static SingletonDemo getInstance() {
        if (instance == null) {
            synchronized (SingletonDemo.class) {
                if (instance == null) {
                    instance = new SingletonDemo();
                }
            }
        }
        return instance;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                SingletonDemo.getInstance();
            }, String.valueOf(i)).start();
        }
    }
}
