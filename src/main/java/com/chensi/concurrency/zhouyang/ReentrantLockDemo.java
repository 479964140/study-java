package com.chensi.concurrency.zhouyang;

public class ReentrantLockDemo {


    public static void main(String[] args) {

        MyPhone myPhone = new MyPhone();

        new Thread(() -> {
            myPhone.sendSms();
        }, "t1").start();

        new Thread(() -> {
            myPhone.sendEmail();
        }, "t2").start();
    }
}

class MyPhone {
    public synchronized void sendSms() {
        System.out.println(Thread.currentThread().getName() + "\t sendSms");
        sendEmail();
    }

    public synchronized void sendEmail() {
        System.out.println(Thread.currentThread().getName() + "\t send email");
    }
}
