package com.chensi.concurrency.zhouyang;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author si.chen
 * @date 2020/5/28 13:50
 */
public class VolatileStaticDemo {

    public static AtomicInteger x = new AtomicInteger(0);
    public static volatile int y = 0;

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 100000; i++) {
            new Thread(() -> x.incrementAndGet()).start();
            new Thread(() -> y++).start();
        }

        TimeUnit.SECONDS.sleep(1);

        System.out.println(x.get() + "----" + y);
    }
}
