package com.chensi.concurrency.zhouyang;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class HashMapDemo {

    public static void main(String[] args) {

        //Map<String, Object> hashMap = new HashMap<>(16);
        ConcurrentHashMap hashMap=new ConcurrentHashMap();


        Map<String, Object> hashTable = new Hashtable<>();

        //hashMap.put(null, null);
        //hashTable.put(null, null);


        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                hashMap.put(Thread.currentThread().getName(), UUID.randomUUID().toString().substring(0,8));
                System.out.println(hashMap);
            }, String.valueOf(i)).start();
        }

    }
}
