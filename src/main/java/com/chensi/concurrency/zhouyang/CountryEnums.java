package com.chensi.concurrency.zhouyang;

import lombok.Getter;

@Getter
public enum CountryEnums {
    ONE(1, "韩"),
    TWO(2, "魏"),
    THREE(3, "齐"),
    FOUR(4, "楚"),
    FIVE(5, "燕"),
    SIX(6, "赵");

    private Integer retCode;

    private String retMsg;

    CountryEnums(Integer retCode, String retMsg) {
        this.retCode = retCode;
        this.retMsg = retMsg;
    }

    public static CountryEnums forEachCountryEnums(Integer index) {
        for (CountryEnums item : values()) {
            if (item.retCode.equals(index)) {
                return item;
            }
        }
        return null;
    }
}