package com.chensi.concurrency.zhouyang;

import java.util.concurrent.*;

public class ThreadPoolDemo {

    public static void main(String[] args) {

        //ExecutorService executorService = Executors.newFixedThreadPool(5);
        //ExecutorService executorService = Executors.newSingleThreadExecutor();
        //ExecutorService executorService = Executors.newCachedThreadPool();
        ExecutorService executorService = new ThreadPoolExecutor(2, 5, 1L,
                TimeUnit.MINUTES, new LinkedBlockingQueue<>(3), Executors.defaultThreadFactory());


        try {
            for (int i = 0; i < 8; i++) {
                executorService.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "在执行");
                });
            }
        } finally {
            executorService.shutdown();
        }

    }


}
