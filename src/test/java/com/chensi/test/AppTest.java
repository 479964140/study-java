package com.chensi.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 测试模板--带容器
 *
 * @author si.chen
 * @date 2020/5/28 13:38
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AppTest {
}
